package com.ctl.cache.config;


import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
//import com.fasterxml.jackson.core.JsonGenerator;
//import com.fasterxml.jackson.databind.DeserializationFeature;
//import com.fasterxml.jackson.databind.module.SimpleModule;
//import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

/**
 * <p>Title: JacksonMapper</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2019</p>
 * <p>Company: www.hanshow.com</p>
 *
 * @author ctl
 * @version 1.0
 * @date 2020-04-15 22:11
 */
public class JacksonMapper extends ObjectMapper {
    public JacksonMapper() {
        super();
        this.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        this.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);


//        super();
//        this.configure(JsonGenerator.Feature.WRITE_BIGDECIMAL_AS_PLAIN, true);
//        this.configure(JsonGenerator.Feature.IGNORE_UNKNOWN, true);
//        this.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
//        this.setSerializationInclusion(JsonInclude.Include.ALWAYS);
//
//        SimpleModule simpleModule = new SimpleModule();
//        simpleModule.addSerializer(Long.class, ToStringSerializer.instance);
//        simpleModule.addSerializer(Long.TYPE, ToStringSerializer.instance);
//        simpleModule.addSerializer(long.class, ToStringSerializer.instance);
//        registerModule(simpleModule);
    }
}
