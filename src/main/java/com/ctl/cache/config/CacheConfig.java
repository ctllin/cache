package com.ctl.cache.config;


import com.google.common.cache.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

@Slf4j
@Configuration
public class CacheConfig implements ApplicationRunner {
    @Resource
    private RedisTemplate redisTemplate;

    public void run(ApplicationArguments args) {
        log.info("=================================================args={}", args);
        log.info("=================================================times={}", redisTemplate.opsForValue().increment("times"));
    }


    @Bean(name = "guavaCache")
    public Cache<String, Object> guavaCache() {
        Cache<String, Object> cache = CacheBuilder.newBuilder()
                .maximumSize(100) // 设置缓存的最大容量
                .expireAfterWrite(5, TimeUnit.SECONDS) // 设置缓存在写入一分钟后失效
                .concurrencyLevel(1) // 设置并发级别为10
                .recordStats() // 开启缓存统计
                .build();
        return cache;
    }

    @Bean(name = "timeCache")
    public LoadingCache<Integer, Long> getTime() {
        LoadingCache<Integer, Long> timeCache = CacheBuilder.newBuilder()
                //设置并发级别为10，并发级别是指可以同时写缓存的线程数
                .concurrencyLevel(10)
                //设置写缓存后60秒钟过期
                .expireAfterWrite(60, TimeUnit.SECONDS)
                //设置缓存容器的初始容量为10
                .initialCapacity(10)
                //设置缓存最大容量为100，超过100之后就会按照LRU最近虽少使用算法来移除缓存项
                .maximumSize(2000)
                //设置要统计缓存的命中率
                .recordStats()
                //设置缓存的移除通知
                .removalListener((notification) ->{
                    //如果是长期未使用超时,调用时才会执行此方法
                    log.info("guava_cache key={},was removed, cause is {}  ", notification.getKey(), notification.getCause());
                })
                //build方法中可以指定CacheLoader，在缓存不存在时通过CacheLoader的实现自动加载缓存
                .build(new CacheLoader<Integer, Long>() {
                    @Override
                    public Long load(Integer key) throws Exception {
                        log.info("guava_cache load key={}", key);
                        return System.currentTimeMillis();
                    }
                });
        return timeCache;
    }

}