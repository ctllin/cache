package com.ctl.cache.controller;

import com.ctl.cache.model.User;
import com.ctl.cache.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;
    /**
     * 新增
     * @param request
     * @param record
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST, consumes = "application/json;charset=utf-8")
    @ResponseBody
    public Object add(HttpServletRequest request, @RequestBody User record) {
        return userService.add(record);
    }

    /**
     * 编辑
     * @param request
     * @param record
     * @return
     */
    @RequestMapping(value = "/modify", method = RequestMethod.POST, consumes = "application/json;charset=utf-8")
    @ResponseBody
    public Object modify(HttpServletRequest request, @RequestBody User record) {
        return userService.modify(record);
    }

    /**
     * 详情
     * @param request
     * @param record
     * @return
     */
    @RequestMapping(value = "/detail", method = RequestMethod.POST, consumes = "application/json;charset=utf-8")
    @ResponseBody
    public Object detail(HttpServletRequest request, @RequestBody User record) {
        return userService.detail(record.getId());
    }

    /**
     * 列表
     *
     * @param request
     * @param record
     * @return
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST, consumes = "application/json;charset=utf-8")
    @ResponseBody
    public Object list(HttpServletRequest request, @RequestBody User record) {
        return userService.list(record);
    }

    /**
     * 删除
     *
     * @param request
     * @param record
     * @return
     */
    @RequestMapping(value = "/delete", method = RequestMethod.POST, consumes = "application/json;charset=utf-8")
    @ResponseBody
    public Object delete(HttpServletRequest request, @RequestBody User record) {
        return userService.delete(record.getId());
    }
}
