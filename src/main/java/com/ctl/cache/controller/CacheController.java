package com.ctl.cache.controller;

import com.alibaba.fastjson.JSON;
import com.ctl.cache.model.User;
import com.ctl.cache.service.UserService;
import com.google.common.cache.LoadingCache;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

@Slf4j
@RestController
public class CacheController {
    //guavaCache
    @Autowired
    @Qualifier("timeCache")
    private LoadingCache<Integer, Long> cache;
    //ehcache或者redis
    @Autowired
    private CacheManager cacheManager;
    @Autowired
    private UserService userService;

    //    @Autowired(required = false)
//    private EhCacheCacheManager ehCacheCacheManager;
    @RequestMapping("/test1/{day}")
    public Object test1(@PathVariable(value = "day") Integer day) {
        Long times = null;
        try {
            log.info("guava_cache values={}", cache.asMap());
            times = cache.get(day);
            log.info("guava_cache time={}", times);
            return times;
        } catch (ExecutionException e) {
            log.info(times.toString());
        }
        return -1;
    }

    @RequestMapping("/test2/{id}")
    public Object test2(@PathVariable(value = "id") Integer id) {
        Cache.ValueWrapper valueW = cacheManager.getCache("users").get(id);
        if (valueW == null) {
            User user = new User();
            user.setId(id.longValue());
            user.setName("NC" + System.currentTimeMillis());
            cacheManager.getCache("users").put(user.getId(), user);
            return JSON.toJSONString(user);
        } else {
            return JSON.toJSONString(valueW.get());
        }

    }

    @RequestMapping("/test3/{id}")
    public Object test3(User userP) {
        //验证@Cacheable注解
        //com.ctl.cache.config.RedisConfig cacheManager 注释掉会执行ehcache,放开会使用redis
        return userService.findById(userP.getId());
    }

    @RequestMapping("/test4/{id}")
    public Object test4(User userP) {
        //验证@Cacheable注解
        //com.ctl.cache.config.RedisConfig cacheManager 注释掉会执行ehcache,放开会使用redis
        Map<String, Object> queryMap = new HashMap<>();
        queryMap.put("id", userP.getId());
        return userService.findUser(queryMap);
    }
}
