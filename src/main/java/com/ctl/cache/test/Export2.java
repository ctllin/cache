package com.ctl.cache.test;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.alibaba.excel.support.ExcelTypeEnum;
import lombok.Builder;

import java.io.File;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.util.*;

/**
 * <p>Title: SignalsExportDTO</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2019</p>
 * <p>Company: www.hanshow.com</p>
 * 通过反射，动态修改注解的某个属性值
 *
 * @author guolin
 * @version 1.1
 * @date 2020-06-12 11:42
 */

@Builder
@HeadRowHeight(value = 25)
public class Export2 implements Serializable {
    public String getSignalsRuleName() {
        return signalsRuleName;
    }

    public void setSignalsRuleName(String signalsRuleName) {
        this.signalsRuleName = signalsRuleName;
    }

    public String getSignalsName() {
        return signalsName;
    }

    public void setSignalsName(String signalsName) {
        this.signalsName = signalsName;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public Export2() {
    }

    public Export2(String signalsRuleName, String signalsName, String storeName) {
        this.signalsRuleName = signalsRuleName;
        this.signalsName = signalsName;
        this.storeName = storeName;
    }

    @ExcelProperty(value = "计划名称", index = 0)
    @ColumnWidth(value = 20)
    private String signalsRuleName;

    @ExcelProperty(value = "规则名称", index = 1)
    @ExcelI18N(i18nkey = "${signal.name}")
    @ColumnWidth(value = 20)
    private String signalsName;


    @ExcelProperty(value = "门店名称", index = 2)
    @ColumnWidth(value = 20)
    private String storeName;

    public synchronized static void test(Locale locale,Class clazz) throws NoSuchFieldException, IllegalAccessException {
        if(locale==null){
            locale=Locale.CHINA;
        }
        Field[] fields = clazz.getDeclaredFields();
        for (int j = 0; j < fields.length; j++) {
            Field field = fields[j];
            if (field.isAnnotationPresent(ExcelI18N.class) && field.isAnnotationPresent(ExcelProperty.class)) {
                //获取SignalsRuleExportDTO字段上的ExcelProperty注解实例
                ExcelI18N i18n = field.getAnnotation(ExcelI18N.class);
                ExcelProperty excel = field.getAnnotation(ExcelProperty.class);
                //获取 ExcelProperty 这个代理实例所持有的 InvocationHandler
                InvocationHandler i18nH = Proxy.getInvocationHandler(i18n);
                InvocationHandler excelH = Proxy.getInvocationHandler(excel);
                // 获取 AnnotationInvocationHandler 的 memberValues 字段
                Field i18nF = i18nH.getClass().getDeclaredField("memberValues");
                Field excelF = excelH.getClass().getDeclaredField("memberValues");
                // 因为这个字段事 private final 修饰，所以要打开权限
                i18nF.setAccessible(true);
                excelF.setAccessible(true);
                // 获取 memberValues
                Map i18nValues = (Map) i18nF.get(i18nH);
                Map excelValues = (Map) excelF.get(excelH);
                // 修改 value 属性值
                Object i18nO = i18nValues.get("i18nkey");
                Object excelOS = excelValues.get("value");
                if (i18nO != null && excelOS != null) {
                    String i18nv = (String) i18nO;
                    String i18nStr = null;
                    if (i18nv.startsWith("${") && i18nv.endsWith("}")) {
                        ResourceBundle rb = ResourceBundle.getBundle("message", locale);
//                            ResourceBundle rb = ResourceBundle.getBundle("message", Locale.CHINA);
                        i18nStr = rb.getString(i18nv.substring(2, i18nv.length() - 1));
                    }
                    if(i18nStr!=null&&!"".equals(i18nStr.trim())){
                        excelValues.put("value", new String[]{i18nStr});
                    }
                }
            }
        }

    }

    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {
        List<Export2> list = new ArrayList<>();
        Export2 signalsRuleExportDTO = new Export2();
        list.add(signalsRuleExportDTO);
        signalsRuleExportDTO.setStoreName("测试门店");
        signalsRuleExportDTO.setSignalsName("测试计划");
        test(Locale.US,Export2.class);
        EasyExcel.write(new File("/home/ctl/1.xlsx"), Export2.class).excelType(ExcelTypeEnum.XLSX).sheet("detail").doWrite(list);
        test(Locale.CHINA,Export2.class);
        EasyExcel.write(new File("/home/ctl/2.xlsx"), Export2.class).excelType(ExcelTypeEnum.XLSX).sheet("detail").doWrite(list);
    }
}
