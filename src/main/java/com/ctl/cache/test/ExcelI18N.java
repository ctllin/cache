package com.ctl.cache.test;

import java.lang.annotation.*;


/**
 * <p>Title: ExcelI18N</p>
 * <p>Description: /p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: www.ctl.com</p>
 *
 * @author guolin
 * @version 1.0
 * @date 2019-03-19 10:38
 */
@Documented
@Inherited
@Target({ ElementType.FIELD, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
public @interface ExcelI18N {
    String i18nkey();
}
