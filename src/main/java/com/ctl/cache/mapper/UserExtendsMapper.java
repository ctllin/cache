package com.ctl.cache.mapper;

import com.ctl.cache.model.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.cache.annotation.CacheEvict;

import java.util.List;

@Mapper
public interface UserExtendsMapper {
    @CacheEvict(value = {"userList"}, key = "#record.id+'-'+#record.name", cacheManager = "redisCacheManager")
    List<User> list(@Param("record") User record);
}