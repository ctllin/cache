package com.ctl.cache.mapper;

import com.ctl.cache.model.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;

@Mapper
public interface UserMapper {
    /**
     * {@link com.ctl.cache.service.UserService#detail(Long)}
     * {@link com.ctl.cache.service.UserService#list(User)}
     * @param id
     * @return
     */
    @Caching(evict = {@CacheEvict(value = {"user", "userDetail"}, key = "#id", cacheManager = "redisCacheManager"),
            @CacheEvict(cacheNames = {"users"}, key = "#record.id+'-'+#record.name", cacheManager = "redisCacheManager")
    })
    //@CacheEvict(value = {"user","userDetail"}, key = "#id", cacheManager = "redisCacheManager")
    int deleteByPrimaryKey(Long id);

    /**
     * {@link com.ctl.cache.service.UserService#list(User)}
     * @param record
     * @return
     */
    @CacheEvict(cacheNames = {"users"}, key = "#record.id+'-'+#record.name", cacheManager = "redisCacheManager", allEntries = true)
    //@CachePut(cacheNames = {"user"}, key = "#record.id", cacheManager = "redisCacheManager")
    int insert(User record);

    /**
     * {@link com.ctl.cache.service.UserService#list(User)}
     * @param record
     * @return
     */
    @CacheEvict(cacheNames = {"users"}, key = "#record.id+'-'+#record.name", cacheManager = "redisCacheManager", allEntries = true)
    int insertSelective(User record);

    /**
     * {@link com.ctl.cache.service.UserService#detail(Long)}
     * @param id sync 和 unless不共用
     * @return
     */
    @Cacheable(cacheNames = {"user"}, key = "#id", cacheManager = "redisCacheManager", unless = "#result==null || #result.id==null")
    //@Cacheable(cacheNames = {"user"}, sync = true, key = "#id", cacheManager = "redisCacheManager")
    User selectByPrimaryKey(Long id);

    //@CacheEvict(cacheNames = {"user","userDetail"}, key = "#record.id", cacheManager = "redisCacheManager")
    @Caching(evict = {@CacheEvict(value = {"user", "userDetail"}, key = "#record.id", cacheManager = "redisCacheManager"),
            @CacheEvict(cacheNames = {"users"}, key = "#record.id+'-'+#record.name", cacheManager = "redisCacheManager")
    })
    int updateByPrimaryKeySelective(User record);
    //@CacheEvict(cacheNames = {"user","userDetail"}, key = "#record.id", cacheManager = "redisCacheManager")
    @Caching(evict = {@CacheEvict(value = {"user", "userDetail"}, key = "#id", cacheManager = "redisCacheManager"),
            @CacheEvict(cacheNames = {"users"}, key = "#record.id+'-'+#record.name", cacheManager = "redisCacheManager")
    })
    int updateByPrimaryKey(User record);

}