package com.ctl.cache.service.impl;

import com.alibaba.fastjson.JSON;
import com.ctl.cache.mapper.UserExtendsMapper;
import com.ctl.cache.mapper.UserMapper;
import com.ctl.cache.model.User;
import com.ctl.cache.service.UserService;
import com.ctl.cache.util.SnowflakeIdUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Random;

@Slf4j
@Service
public class UserServiceImpl implements UserService {
    @Autowired(required = false)
    private UserMapper userMapper;
    @Autowired(required = false)
    private UserExtendsMapper userExtendsMapper;

    //缓存的同步 sync：
    //在多线程环境下，某些操作可能使用相同参数同步调用。默认情况下，缓存不锁定任何资源，可能导致多次计算，而违反了缓存的目的。对于这些特定的情况，属性 sync 可以指示底层将缓存锁住，使只有一个线程可以进入计算，而其他线程堵塞，直到返回结果更新到缓存中。
    @Cacheable(cacheNames = {"users"}, sync = true, key = "#id", cacheManager = "redisCacheManager")
    public User findById(Long id) {
        log.info("redis_cache load id={}", id);
        User user = new User();
        user.setId(id);
        user.setName("B" + System.currentTimeMillis());
        //return saveUser(user);
        return user;
    }

    @CachePut(cacheNames = {"users"}, key = "#id", cacheManager = "redisCacheManager")
    public User saveUser(User user) {
        if (user == null) {
            user = new User();
            user.setId(new Random().nextLong());
            user.setName("NC" + System.currentTimeMillis());
        }
        return user;
    }

    @CacheEvict(value = {"users"}, key = "#user.id", cacheManager = "redisCacheManager")
    public User deleteById(User user) {
        log.info("delelte id={}", user.getId());
        return user;
    }

    @CacheEvict(value = {"users"}, allEntries = true, cacheManager = "redisCacheManager")
    public void reloadAll() {
        log.info("reloadAll");
    }

    @Cacheable(cacheNames = "users", key = "#map['id']", cacheManager = "ehCacheCacheManager")
    public User findUser(Map<String, Object> map) {
        log.info("ehcache_cache load id={}", map.get("id"));
        User user = new User();
        user.setId(new Random().nextLong());
        user.setName("BC" + System.currentTimeMillis());
        return user;
    }

    @Override
    public Integer add(User record) {
        log.info("add={}", JSON.toJSONString(record));
        if (userMapper.selectByPrimaryKey(record.getId()) != null) {
            record.setId(SnowflakeIdUtils.genID());
            record.setName("SNOW" + record.getId());
        }
        return userMapper.insertSelective(record);
    }

    @Override
    public Integer modify(User record) {
        log.info("modify={}", JSON.toJSONString(record));
        return userMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public User detail(Long id) {
        log.info("id={}", id);
        User user = userMapper.selectByPrimaryKey(id);
        if (user != null) {
            user.setEmail(SnowflakeIdUtils.genID() + "@163.com");
        }
        return user;
    }

    @Override
    public List<User> list(User record) {
        log.info("list={}", JSON.toJSONString(record));
        return userExtendsMapper.list(record);
    }

    @Override
    public Object delete(Long id) {
        log.info("delete={}", id);
        return userMapper.deleteByPrimaryKey(id);
    }

}
