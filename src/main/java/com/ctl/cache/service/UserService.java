package com.ctl.cache.service;

import com.ctl.cache.mapper.UserMapper;
import com.ctl.cache.model.User;
import org.springframework.cache.annotation.Cacheable;

import java.util.Map;
import java.util.List;

public interface UserService {
    User findById(Long id);

    User saveUser(User user);

    User deleteById(User user);

    void reloadAll();

    User findUser(Map<String, Object> map);

    Integer add(User record);

    Integer modify(User record);

    /**
     * {@link UserMapper#selectByPrimaryKey(Long)}
     * 返回空不缓存
     * sync 和 unless不共用
     * @param id
     * @return
     */
    @Cacheable(cacheNames = {"userDetail"}, key = "#id", cacheManager = "redisCacheManager", unless = "#result==null || #result.id==null")
    //@Cacheable(cacheNames = {"userDetail"}, sync = true, key = "#id", cacheManager = "redisCacheManager")
    User detail(Long id);

    /**
     * list 空不缓存
     *
     * @param record
     * @return
     */
    @Cacheable(cacheNames = {"users"}, key = "#record.id+'-'+#record.name", cacheManager = "redisCacheManager", unless = "#result==null || #result.size()==0")
    List<User> list(User record);

    Object delete(Long id);
}
